<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model modules\atm\models\AtmSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="atm-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'id') ?>

    <?= $form->field($model, 'type') ?>

    <?= $form->field($model, 'cityRU') ?>

    <?= $form->field($model, 'cityUA') ?>

    <?= $form->field($model, 'cityEN') ?>

    <?php // echo $form->field($model, 'fullAddressRu') ?>

    <?php // echo $form->field($model, 'fullAddressUa') ?>

    <?php // echo $form->field($model, 'fullAddressEn') ?>

    <?php // echo $form->field($model, 'placeRu') ?>

    <?php // echo $form->field($model, 'placeUa') ?>

    <?php // echo $form->field($model, 'latitude') ?>

    <?php // echo $form->field($model, 'longitude') ?>

    <?php // echo $form->field($model, 'tw') ?>

    <?php // echo $form->field($model, 'status') ?>

    <?php // echo $form->field($model, 'created_at') ?>

    <?php // echo $form->field($model, 'updated_at') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-outline-secondary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
