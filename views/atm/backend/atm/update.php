<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model modules\atm\models\Atm */

$this->title = 'Изменить: ' . $model->cityRU;
$this->params['breadcrumbs'][] = ['label' => 'Банкоматы', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->cityRU, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Изменить';
?>
<div class="atm-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
