<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model modules\atm\models\Atm */

$this->title = $model->cityRU;
$this->params['breadcrumbs'][] = ['label' => 'Банкоматы', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="atm-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Изменить', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Удалить', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'type',
            'cityRU',
            'cityUA',
            'cityEN',
            'fullAddressRu',
            'fullAddressUa',
            'fullAddressEn',
            'placeRu',
            'placeUa',
            'latitude',
            'longitude',
            //'tw',
            'status',
            'created_at',
            'updated_at',
        ],
    ]) ?>

</div>
