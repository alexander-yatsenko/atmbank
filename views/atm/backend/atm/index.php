<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel modules\atm\models\AtmSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Банкоматы';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="row">
    <div class="col-12">

    <h1><?= Html::encode($this->title) ?></h1>

    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            //'type',
            'cityRU',
           // 'cityUA',
           // 'cityEN',
            'fullAddressRu',
            //'fullAddressUa',
            //'fullAddressEn',
            //'placeRu',
            //'placeUa',
            //'latitude',
            //'longitude',
            //'tw',
            'status',
            'created_at',
            'updated_at',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
    </div>

</div>
