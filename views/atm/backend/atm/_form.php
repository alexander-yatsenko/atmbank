<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model modules\atm\models\Atm */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="atm-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'type')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'cityRU')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'cityUA')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'cityEN')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'fullAddressRu')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'fullAddressUa')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'fullAddressEn')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'placeRu')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'placeUa')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'latitude')->textInput(['maxlength' => true, 'disabled' => true]) ?>

    <?= $form->field($model, 'longitude')->textInput(['maxlength' => true, 'disabled' => true]) ?>

    <?= $form->field($model, 'status')->dropDownList([ 'on' => 'On', 'off' => 'Off', ], ['prompt' => '']) ?>

    <div class="form-group">
        <?= Html::submitButton('Сохранить', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
