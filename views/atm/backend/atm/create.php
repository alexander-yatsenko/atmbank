<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model modules\atm\models\Atm */

$this->title = 'Добавить';
$this->params['breadcrumbs'][] = ['label' => 'Банкоматы', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="atm-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
