<?php

namespace modules\atm\models;

use Yii;

/**
 * This is the model class for table "{{%atm}}".
 *
 * @property int $id
 * @property string $type
 * @property string $cityRU
 * @property string $cityUA
 * @property string $cityEN
 * @property string $fullAddressRu
 * @property string $fullAddressUa
 * @property string $fullAddressEn
 * @property string $placeRu
 * @property string $placeUa
 * @property string $latitude
 * @property string $longitude
 * @property string $tw
 * @property string $status
 * @property string $created_at
 * @property string $updated_at
 */
class Atm extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return '{{%atm}}';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['type', 'cityRU', 'cityUA', 'cityEN', 'fullAddressRu', 'fullAddressUa', 'fullAddressEn', 'placeRu', 'placeUa', 'latitude', 'longitude', 'tw'], 'required'],
            [['status'], 'string'],
            [['created_at', 'updated_at'], 'safe'],
            [['type', 'cityRU', 'cityUA', 'cityEN', 'fullAddressRu', 'fullAddressUa', 'fullAddressEn', 'placeRu', 'placeUa', 'latitude', 'longitude'], 'string', 'max' => 255],
            [['tw'], 'string', 'max' => 1000],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'type' => 'Type',
            'cityRU' => 'Город Ru',
            'cityUA' => 'Город Ua',
            'cityEN' => 'Город En',
            'fullAddressRu' => 'Адрес Ru',
            'fullAddressUa' => 'Адрес Ua',
            'fullAddressEn' => 'Адрес En',
            'placeRu' => 'Place Ru',
            'placeUa' => 'Place Ua',
            'latitude' => 'Latitude',
            'longitude' => 'Longitude',
            'tw' => 'Tw',
            'status' => 'Статус',
            'created_at' => 'Создано',
            'updated_at' => 'Изменено',
        ];
    }
}
