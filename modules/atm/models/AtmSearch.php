<?php

namespace modules\atm\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use modules\atm\models\Atm;

/**
 * AtmSearch represents the model behind the search form of `modules\atm\models\Atm`.
 */
class AtmSearch extends Atm
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id'], 'integer'],
            [['type', 'cityRU', 'cityUA', 'cityEN', 'fullAddressRu', 'fullAddressUa', 'fullAddressEn', 'placeRu', 'placeUa', 'latitude', 'longitude', 'tw', 'status', 'created_at', 'updated_at'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Atm::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
        ]);

        $query->andFilterWhere(['like', 'type', $this->type])
            ->andFilterWhere(['like', 'cityRU', $this->cityRU])
            ->andFilterWhere(['like', 'cityUA', $this->cityUA])
            ->andFilterWhere(['like', 'cityEN', $this->cityEN])
            ->andFilterWhere(['like', 'fullAddressRu', $this->fullAddressRu])
            ->andFilterWhere(['like', 'fullAddressUa', $this->fullAddressUa])
            ->andFilterWhere(['like', 'fullAddressEn', $this->fullAddressEn])
            ->andFilterWhere(['like', 'placeRu', $this->placeRu])
            ->andFilterWhere(['like', 'placeUa', $this->placeUa])
            ->andFilterWhere(['like', 'latitude', $this->latitude])
            ->andFilterWhere(['like', 'longitude', $this->longitude])
            ->andFilterWhere(['like', 'tw', $this->tw])
            ->andFilterWhere(['like', 'status', $this->status]);

        return $dataProvider;
    }
}
