<?php

namespace console\controllers;

use Yii;
use yii\console\Controller;

class RbacController extends Controller
{

    public function actionInit()
    {
        $auth = Yii::$app->authManager;
        
        $auth->removeAll();

        $publicAction = $auth->createPermission('publicAction');
        $publicAction->description = 'Публичные действия для пользователя';
        $auth->add($publicAction);
        
        $privateAction = $auth->createPermission('privateAction');
        $privateAction->description = 'Приватные действия для пользователя';
        $auth->add($privateAction);
        
        $admin = $auth->createRole('admin');
        $moderator = $auth->createRole('moderator');
        
        $auth->add($admin);
        $auth->add($moderator);
        
        $auth->addChild($admin, $publicAction);
        $auth->addChild($admin, $privateAction);
        
        $auth->addChild($moderator, $publicAction);
        $auth->addChild($moderator, $privateAction);
    }
}