<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%atm}}`.
 */
class m190601_170015_create_atm_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%atm}}', [
            'id' => $this->primaryKey(),
            'type' => $this->string(255)->notNull(),
            'cityRU' => $this->string(255)->notNull(),
            'cityUA' => $this->string(255)->notNull(),
            'cityEN' => $this->string(255)->notNull(),
            'fullAddressRu' => $this->string(255)->notNull(),
            'fullAddressUa' => $this->string(255)->notNull(),
            'fullAddressEn' => $this->string(255)->notNull(),
            'placeRu' => $this->string(255)->notNull(),
            'placeUa' => $this->string(255)->notNull(),
            'latitude' => $this->string(255)->notNull(),
            'longitude' => $this->string(255)->notNull(),
            'tw' => $this->string(1000)->notNull(),
            'status' => 'ENUM("on", "off") NOT NULL DEFAULT "on"',
            'created_at' => $this->timestamp()->notNull()->defaultExpression('CURRENT_TIMESTAMP'),
            'updated_at' => $this->timestamp()->notNull()->defaultExpression('CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP')
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%atm}}');
    }
}
