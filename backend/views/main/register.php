<?php

use yii\helpers\Url;
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

$this->title = 'Регистрация';
$this->params['breadcrumbs'][] = $this->title;

$this->registerJsFile(Yii::$app->request->BaseUrl . '/js/vendor/bootstrap.bundle.min.js', ['depends' => [yii\web\JqueryAsset::className()]]);
$this->registerJsFile(Yii::$app->request->BaseUrl . '/js/dore.script.js', ['depends' => [yii\web\JqueryAsset::className()]]);
$this->registerJsFile(Yii::$app->request->BaseUrl . '/js/scripts.js', ['depends' => [yii\web\JqueryAsset::className()]]);
?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <title></title>
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <link rel="stylesheet" href="<?=Url::to('/font/iconsmind/style.css')?>" />
    <link rel="stylesheet" href="<?=Url::to('/font/simple-line-icons/css/simple-line-icons.css')?>" />

    <link rel="stylesheet" href="<?=Url::to('/css/vendor/bootstrap.min.css')?>" />
    <link rel="stylesheet" href="<?=Url::to('/css/vendor/bootstrap-float-label.min.css')?>" />
    <link rel="stylesheet" href="<?=Url::to('/css/main.css')?>" />
</head>

<body class="background show-spinner">
    <div class="fixed-background"></div>
    <main>
        <div class="container">
            <div class="row h-100">
                <div class="col-12 col-md-10 mx-auto my-auto">
                    <div class="card auth-card">
                        <div class="position-relative image-side ">

                        </div>
                        <div class="form-side">
                            <a href="<?=Url::home();?>">
                                <span class="logo-single"></span>
                            </a>
                            <h6 class="mb-4"><?= Html::encode($this->title) ?></h6>
                            <?php $form = ActiveForm::begin(['id' => 'login-form']); ?>
                                <label class="form-group has-float-label mb-4">
                                    <?= $form->field($model, 'username')->textInput(['autofocus' => true]) ?>
                                </label>
                            
                                <label class="form-group has-float-label mb-4">
                                    <?= $form->field($model, 'email')->textInput() ?>
                                </label>

                                <label class="form-group has-float-label mb-4">
                                       <?= $form->field($model, 'password')->passwordInput() ?>
                                </label>
                            
                                <label class="form-group has-float-label mb-4">
                                       <?= $form->field($model, 'role')->dropDownList([
                                            '' => 'Выберите',
                                            'admin' => 'Админ',
                                            'moderator'=>'Модератор'
                                        ]) ?>
                                </label>
                       
                                <div class="d-flex justify-content-between align-items-center">
                                    <?=Html::a('Войти', Url::to('login'))?>
                                    <?=Html::submitButton('РЕГИСТРАЦИЯ', ['class' => 'btn btn-primary btn-lg btn-shadow'])?>
                                </div>
                            <?php ActiveForm::end(); ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </main>
 
</body>

</html>