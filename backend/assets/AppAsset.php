<?php

namespace backend\assets;

use yii\web\AssetBundle;

/**
 * Main backend application asset bundle.
 */
class AppAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';
    public $css = [
        '/font/iconsmind/style.css',
        '/font/simple-line-icons/css/simple-line-icons.css',
        '/css/vendor/bootstrap.min.css',
        '/css/vendor/perfect-scrollbar.css',
        '/css/main.css',
        'css/dore.light.blue.min.css',
    ];
    public $js = [
        //'/js/vendor/jquery-3.3.1.min.js',
        '/js/vendor/bootstrap.bundle.min.js',
        '/js/vendor/perfect-scrollbar.min.js',
        '/js/vendor/mousetrap.min.js',
        '/js/dore.script.js',
        '/js/scripts.single.theme.js',
        
    ];
    public $depends = [
        'yii\web\YiiAsset',
        'yii\bootstrap\BootstrapAsset',
    ];
}