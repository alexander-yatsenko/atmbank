<?php
$params = array_merge(
    require __DIR__ . '/../../common/config/params.php',
    require __DIR__ . '/../../common/config/params-local.php',
    require __DIR__ . '/params.php',
    require __DIR__ . '/params-local.php'
);

return [
    'id' => 'app-backend',
    'basePath' => dirname(__DIR__),
    'controllerNamespace' => 'backend\controllers',
    'homeUrl' => '/adm',
    'language' => 'ru-RU',
    'bootstrap' => ['log'],
    'modules' => [
        'atm' => [
            'class' => 'modules\atm\Atm',
            'controllerNamespace' => 'modules\atm\controllers\backend',
            'viewPath' => '@views/atm/backend',
        ],
    ],
    'components' => [
        'atm' => [
            'class' => 'common\components\ParseAtm'
        ],
        'request' => [
            'csrfParam' => '_csrf-backend',
            'baseUrl' => '/adm',
        ],
        'user' => [
            'identityClass' => 'common\models\User',
            'enableAutoLogin' => true,
            'identityCookie' => ['name' => '_identity-backend', 'httpOnly' => true],
            'loginUrl' => [
                'main/login'
            ],
        ],
        'session' => [
            // this is the name of the session cookie used for login on the backend
            'name' => 'advanced-backend',
        ],
        'log' => [
            'traceLevel' => YII_DEBUG ? 3 : 0,
            'targets' => [
                [
                    'class' => 'yii\log\FileTarget',
                    'levels' => ['error', 'warning'],
                ],
                [
                    'class' => 'yii\log\FileTarget',
                    'categories' => ['backend'],
                    'logFile' => '@runtime/logs/backend.log',
                    'levels' => ['error', 'warning', 'info'],
                    'logVars' => [],
                ],
            ],
        ],
        'errorHandler' => [
            'errorAction' => 'main/error',
        ],

        'urlManager' => [
            'enablePrettyUrl' => true,
            'showScriptName' => false,
            'enableStrictParsing' => false,
            'rules' => [
            ],
        ],
        'authManager' => [
            'class' => 'yii\rbac\DbManager',
        ],
     
    ],
    'params' => $params,
    'defaultRoute' => 'main/index',
];
