## Atm Bank

**Написать проект на Yii2 Advanced**

Интегрироваться с API [https://api.privatbank.ua/#p24/atm](https://api.privatbank.ua/#p24/atm)

**Задание**

Добавить 3 роли:

* Администратор - по нажатию кнопки обновляет список банкоматов и сохраняет в базе данных.
* Модератор - может удалять или редактировать данные по конкретному банкомату.
* Пользователь - может просматривать список банкоматов на карте, или списком. Группировать по городу. Искать по названию или улице.

**Установка проекта**

* git clone [https://alexander-yatsenko@bitbucket.org/alexander-yatsenko/atmbank.git](https://alexander-yatsenko@bitbucket.org/alexander-yatsenko/atmbank.git)
* php init
* composer install
* php yii migrate
* php yii migrate --migrationPath=@yii/rbac/migrations
* php yii rbac/init

**Панель управления**

URL /adm

Чтобы войти в панель управления, необходимо зарегистрировать пользователя в роли админ и модератор.
После этого будут доступны функции для этих ролей как описано в задании. 

## DEMO

[http://atm.green-hectares.com.ua/](http://atm.green-hectares.com.ua/)
