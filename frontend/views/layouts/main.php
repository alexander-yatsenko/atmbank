<?php

/* @var $this \yii\web\View */
/* @var $content string */

use yii\helpers\Html;
use frontend\assets\AppAsset;
use yii\helpers\Url;

AppAsset::register($this);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">

<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?php $this->registerCsrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>
</head>

<body class="show-spinner">
<?php $this->beginBody() ?>
    <div class="landing-page">
        <div class="mobile-menu">
            <a href="<?=Url::home()?>" class="logo-mobile">
                <span></span>
            </a>
        </div>
        <div class="main-container">
            <nav class="landing-page-nav" style="background-color: #145388;">
                <div class="container d-flex align-items-center justify-content-between">
                    <a class="navbar-logo pull-left" href="<?=Url::home()?>">
                        <span class="white"></span>
                        <span class="dark"></span>
                    </a>
                    <ul class="navbar-nav d-none d-lg-flex flex-row">
                        <li class="nav-item mr-3">
                            <a class="btn btn-outline-semi-light btn-sm pr-4 pl-4" href="<?=Url::to('/adm')?>">ПАНЕЛЬ УПРАВЛЕНИЯ</a>
                        </li>
                    </ul>
                </div>
            </nav>
            <div class="content-container" id="home">

                <div class="section">
                    <div class="container" id="content">
                        <div class="row">
                            <div class="col-12 col-lg-12">
                                <?= $content ?>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </div>

<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>