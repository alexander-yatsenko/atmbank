<?php

use yii\widgets\LinkPager;
use yii\helpers\Url;

$this->title = 'Банкоматы в Украине';
?>
<div class="card mb-4">
    <div class="card-body">
        <h5 class="mb-4">Поиск</h5>
        <form>
            <label class="form-group has-float-label">
                <select class="select2-single form-control" name="city">
                    <option value="">--</option>
                    <?php foreach ($city as $key => $item):?>
                    <?php if(Yii::$app->request->get('city') == $item->cityRU): ?>
                        <option value="<?= $item->cityRU ?>" selected><?= $item->cityRU ?></option>
                    <?php else: ?>
                        <option value="<?= $item->cityRU ?>"><?= $item->cityRU ?></option>
                    <?php endif;?>
                    <?php endforeach; ?>
                </select>
                <span>Город</span>
            </label>
            <label class="form-group has-float-label">
                    <input class="form-control" name="address" value="<?=Yii::$app->request->get('address') ? Yii::$app->request->get('address'): ''?>" />
                    <span>Улица</span>
            </label>
            <label class="form-group has-float-label">
                    <input class="form-control" name="place" value="<?=Yii::$app->request->get('place') ? Yii::$app->request->get('place'): ''?>" />
                    <span>Название</span>
            </label>
            <button class="btn btn-primary" type="submit">Искать</button>
        </form>
    </div>
</div>
 <div id="map"></div>
    <?php if(isset($lists) && count($lists)):?>
    <div class="row feature-row">
        <div class="d-none d-md-block col-12">
            <div class="card d-flex flex-row mb-3 table-heading">
                <div class="d-flex flex-grow-1 min-width-zero">
                    <div class="card-body align-self-center d-flex flex-column flex-md-row justify-content-between min-width-zero align-items-md-center">
                        <p class="list-item-heading mb-0 truncate w-20 w-xs-100">
                            <?php 
                            $href = Url::to(['/main/index', 'sort' => 'asc']); 
                            $icon = '<i class="simple-icon-arrow-down"></i>';
                       
                            if(Yii::$app->request->get('sort') == 'asc'){
                                 $href = Url::to(['/main/index', 'sort' => 'desc']); 
                                $icon = '<i class="simple-icon-arrow-up"></i>';
                            }elseif(Yii::$app->request->get('sort') == 'desc'){
                                 $href = Url::to(['/main/index', 'sort' => 'asc']); 
                                $icon = '<i class="simple-icon-arrow-down"></i>';
                            }     
                            ?>
                            <a href="<?=$href?>">
                                Город <?=$icon?>
                            </a>
                        </p>
                        <p class="mb-0 text-primary w-20 w-xs-100 text-center">Название</p>
                        <p class="mb-0 text-primary w-20 w-xs-100 text-center">Адрес</p>
                    </div>
                </div>
            </div>
            <?php foreach ($lists as $key => $list) :?>
            <div class="card d-flex flex-row mb-3">
                <div class="d-flex flex-grow-1 min-width-zero">
                    <div class="card-body align-self-center d-flex flex-column flex-md-row justify-content-between min-width-zero align-items-md-center">
                        <p class="list-item-heading mb-0 truncate w-20 w-xs-100">
                            <?=$list->cityRU?>
                        </p>
                        <p class="mb-0 text-primary w-20 w-xs-100 text-center">
                           <?=$list->placeRu?>
                        </p>
                        <p class="mb-0 text-primary w-20 w-xs-100 text-center">
                           <?=$list->fullAddressRu?>
                        </p>
                    </div>
                </div>
            </div>
            <?php endforeach;?>
            <div class="col-12 text-center">
                <?= LinkPager::widget([
                    'pagination' => $pagination,
                    'options' => ['class' => 'pagination justify-content-center mb-0'],
                    ]) ;
                ?>
             </div>
        </div>
    </div>
    <?php endif;?>
  
<?=$this->render('_viewJs', [
    'model' => $model,
    'cityLocation' => $cityLocation
]);
?>