<?php 
use yii\helpers\Url;
?>
<script defer>
    var libMarkers = [
    <?php foreach ($model as $key => $item):?>
        {
            name: '<?=$item->placeRu?>',
            tw: '<?php
                foreach (unserialize($item->tw) as $key => $value){
                    if($key == 'mon'){
                        $day = 'Пн.';
                    }elseif($key == 'tue'){
                        $day = 'Вт.';
                    }elseif($key == 'wed'){
                        $day = 'Ср.';
                    }elseif($key == 'thu'){
                        $day = 'Чт.';
                    }elseif($key == 'fri'){
                        $day = 'Пт.';
                    }elseif($key == 'sat'){
                        $day = 'Сб.';
                    }elseif($key == 'sun'){
                        $day = 'Нд.';
                    }
                    elseif($key == 'hol'){
                        $day = 'Вых.';
                    }
                    echo $day .': ' .$value . '<br>';
                }
                ?>',
            locate: {
                lat: <?=$item->latitude?>,
                lng: <?=$item->longitude?>
            }
        },
    <?php endforeach;?>
    ];

    function initMap() {
        var cityLocation = {
            lat: <?=$cityLocation['latitude']?>,
            lng: <?=$cityLocation['longitude']?>
        };
        var map = new google.maps.Map(document.getElementById('map'), {
            zoom: 10,
            center: cityLocation,
            streetViewControl: false,
            mapTypeControl: false,
            fullscreenControl: false,
            styles: [{"featureType":"administrative.land_parcel","elementType":"all","stylers":[{"visibility":"off"}]},{"featureType":"landscape.man_made","elementType":"all","stylers":[{"visibility":"off"}]},{"featureType":"poi","elementType":"labels","stylers":[{"visibility":"off"}]},{"featureType":"road","elementType":"labels","stylers":[{"visibility":"simplified"},{"lightness":20}]},{"featureType":"road.highway","elementType":"geometry","stylers":[{"hue":"#f49935"}]},{"featureType":"road.highway","elementType":"labels","stylers":[{"visibility":"simplified"}]},{"featureType":"road.arterial","elementType":"geometry","stylers":[{"hue":"#fad959"}]},{"featureType":"road.arterial","elementType":"labels","stylers":[{"visibility":"off"}]},{"featureType":"road.local","elementType":"geometry","stylers":[{"visibility":"simplified"}]},{"featureType":"road.local","elementType":"labels","stylers":[{"visibility":"simplified"}]},{"featureType":"transit","elementType":"all","stylers":[{"visibility":"off"}]},{"featureType":"water","elementType":"all","stylers":[{"hue":"#a1cdfc"},{"saturation":30},{"lightness":49}]}]
        });

        var markerRed = "<?=Url::to('/img/rose-marker.png', true)?>";

        var infoWin = new google.maps.InfoWindow();

        var markers = libMarkers.map(function (marker, i) {
            var mItem = new google.maps.Marker({
                position: marker.locate,
                icon: markerRed
            });
            google.maps.event.addListener(mItem, 'click', function(evt) {;
                infoWin.setContent('<h3>'+marker.name+'<h3><hr><h4>Время работы:</h4><div>'+marker.tw+'</div>');
                infoWin.open(map, mItem);
            });
            return mItem;
        });

        var markerCluster = new MarkerClusterer(map, markers,
        {imagePath: 'https://developers.google.com/maps/documentation/javascript/examples/markerclusterer/m'});
    }
</script>
<script src="https://developers.google.com/maps/documentation/javascript/examples/markerclusterer/markerclusterer.js"></script>
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDeaxEl6uHA6kSKhQfRtNg4e0mo_BGAkAU&callback=initMap" async defer></script>