$(document).ready(function() {
    if ($().select2) {
        $(".select2-single").select2({
            theme: "bootstrap",
            placeholder: "Введите город",
            maximumSelectionSize: 6,
            containerCssClass: ":all:",
        });
    }
});