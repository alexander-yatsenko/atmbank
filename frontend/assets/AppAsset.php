<?php

namespace frontend\assets;

use yii\web\AssetBundle;

/**
 * Main frontend application asset bundle.
 */
class AppAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';
    public $css = [
        '/font/iconsmind/style.css',
        '/font/simple-line-icons/css/simple-line-icons.css',
        '/css/vendor/bootstrap-stars.css',
        '/css/vendor/bootstrap.min.css',
        '/css/vendor/bootstrap-float-label.min.css',
        '/css/vendor/owl.carousel.min.css',
        '/css/vendor/bootstrap-stars.css',
        '/css/vendor/select2.min.css',
        '/css/main.css',
    ];
    public $js = [
        '/js/vendor/bootstrap.bundle.min.js',
        '/js/vendor/owl.carousel.min.js',
        '/js/vendor/jquery.barrating.min.js',
        '/js/vendor/landing-page/headroom.min.js',
        '/js/vendor/landing-page/jQuery.headroom.js',
        '/js/vendor/landing-page/jquery.scrollTo.min.js',
        '/js/vendor/landing-page/jquery.autoellipsis.js',
        '/js/vendor/select2.full.js',
        '/js/dore.scripts.landingpage.js',
        '/js/scripts.js',
        '/js/select2my.js',    
    ];
    public $depends = [
        'yii\web\YiiAsset',
        'yii\bootstrap\BootstrapAsset',
    ];
}