<?php

namespace common\components;

use Yii;
use yii\base\Component;
use yii\httpclient\Client;
use modules\atm\models\Atm;
use yii\web\ForbiddenHttpException;

class ParseAtm extends Component
{
    const API_URL = 'https://api.privatbank.ua/p24api/infrastructure?json&atm';

    /**
     * @param array $config
     */
    public function __construct($config = [])
    {
        parent::__construct($config);
    }

    /**
     * Get Atm
     * 
     * @return boolean|array
     */
    public function getAtm()
    {
        try {
            $client = new Client();
            $response = $client->createRequest()
                ->setMethod('get')
                ->setUrl(self::API_URL)
                ->send();
            if ($response->isOk) {
                if(isset($response->data['devices'])){
                    return $response->data['devices'];
                }else{
                    Yii::error('Нет данных devices', 'backend');
                    return false;
                }
            }
        } catch (\Exception $e) {
            Yii::error($e->getMessage(), 'backend');
            return false;
        }
    }
    
    /**
     * Save Atm
     * 
     * @return boolean
     * @throws ForbiddenHttpException
     */
    public function saveAtm()
    {
        $devices = $this->getAtm();
        if($devices !== false && is_array($devices)){
            foreach ($devices as $item){
                $transaction = Yii::$app->db->beginTransaction();
                try {
                    $model = Atm::findOne(['latitude' => $item['latitude'], 'longitude' => $item['longitude']]);
                    if($model === null){
                        $model = new Atm();
                        $model->type = $item['type'];
                        $model->cityEN = $item['cityEN'];
                        $model->cityUA = $item['cityUA'];
                        $model->cityRU = $item['cityRU'];
                        $model->fullAddressEn = $item['fullAddressEn'];
                        $model->fullAddressUa = $item['fullAddressUa'];
                        $model->fullAddressRu = $item['fullAddressRu'];
                        $model->placeRu = $item['placeRu'];
                        $model->placeUa = $item['placeUa'];
                        $model->latitude = $item['latitude'];
                        $model->longitude = $item['longitude'];
                        $model->tw = serialize($item['tw']);
                        $model->save();
                    }
                    $transaction->commit();
                } catch (\Throwable $e) {
                    $transaction->rollBack();
                    Yii::error($e, 'backend');
                    return false;
                }
            }
            return true;
        }else{
            throw new ForbiddenHttpException('Нет данных от сервера или ошибка в соединении');
        }
    }
}