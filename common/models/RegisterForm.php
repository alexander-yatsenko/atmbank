<?php
namespace common\models;

use Yii;
use yii\base\Model;

/**
 * Register form
 */
class RegisterForm extends Model
{
    public $username;
    public $email;
    public $role;
    public $password;

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'username' => 'Имя',
            'email' => 'Почта',
            'role' => 'Роль',
            'password' => 'Пароль',
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            // username and password are both required
            [['username', 'password', 'email', 'role'], 'required'],
            ['email', 'trim'],
            ['email', 'required', 'message' => 'Введите E-mail'],
            ['email', 'email', 'message' => 'Некоректний E-mail. Формат: email@domen.com'],
            ['email', 'string', 'max' => 255],
            ['email', 'unique', 'targetClass' => 'common\models\User', 'message' => 'E-mail уже используется в системе'],

            ['password', 'required'],
            ['password', 'string', 'min' => 6],
        ];
    }

    /**
     * Signup
     * 
     * @return \common\models\User|null
     */
    public function signup()
    {
        if (!$this->validate()) {
            return null;
        }
        
        $user = new User();
        $user->username = $this->username;
        $user->email = $this->email;
        $user->setPassword($this->password);
        $user->generateAuthKey();
        
        if($user->save()){
            
            $auth = Yii::$app->authManager;
            $role = $auth->getRole($this->role);
            $auth->assign($role, $user->getId());
            
            return $user;
        }else{
            return null;
        }
    }
}
